﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Oxide.Core.Libraries.Covalence;
using Oxide.Core.Plugins;
using Newtonsoft.Json;

namespace Oxide.Plugins
{
    [Info("AutomatedEvents", "k1lly0u/mspeedie", "0.3.25")]
	// big thank you to Supreme for helping track down the fix for double spawns
    class AutomatedEvents : RustPlugin
    {
        #region Fields

        enum EventType
        {
	        Bradley,
	        CargoPlane,
	        CargoShip,
	        Chinook,
	        Helicopter,
	        HelicopterRefuel,
	        PilotEject,
	        PlaneCrash,
	        XMasEvent,
	        EasterEvent,
	        HalloweenEvent,
	        SantaEvent,
	        None
        }

		DateTime unsetDate = new DateTime(1900, 1, 1, 1, 1, 0);
        private Dictionary<EventType, Timer> eventTimers = new Dictionary<EventType, Timer>();
        const string   permAuto = "automatedevents.allowed";
        const string   permNext = "automatedevents.next";

        #endregion

        #region Oxide Plugins

		[PluginReference] Plugin AlphaChristmas;
		[PluginReference] Plugin BradleyControl;
		[PluginReference] Plugin FancyDrop;
		[PluginReference] Plugin GUIAnnouncements;
		[PluginReference] Plugin HeliControl;
		[PluginReference] Plugin HeliRefuel;
		[PluginReference] Plugin PilotEject;
		[PluginReference] Plugin PlaneCrash;

        #endregion

		private bool   AnnounceOnLoad      = false;
		private bool   doGUIAnnouncements  = false;
		private bool   RestartTimerOnDeath = true;
		private bool   TurnOffEvents       = true;
		private int    StartupDelay        = 60;

        private void LoadDefaultMessages()
        {
            lang.RegisterMessages(new Dictionary<string, string>
            {
			["blankevent"] = "you need to specify an event",
			["bradley"] = "Next Bradley APC expected in approximately {0} minutes",
			["cargoplane"] = "Next Cargo plane expected in approximately {0} minutes",
			["cargoship"] = "Next Cargo ship expected in approximately {0} minutes",
			["chinook"] = "Next Chinook CH47 expected in approximately {0} minutes",
			["christmas"] = "Next Christmas Presents expected in approximately {0} minutes",
			["easter"] = "Next Easter Egg Hunt expected in approximately {0} minutes",
			["halloween"] = "Next Halloween Candy Hunt expected in approximately {0} minutes",
			["helicopter"] = "Next Helicopter expected in approximately {0} minutes",
			["helicopterrefuel"] = "Next Helicopter Refuel expected in approximately {0} minutes",
			["notallowed"] = "You are not allowed to use the '{0}' command",
			["notset"] = "{0} is not set to run via Automated Events",
			["piloteject"] = "Next Helicopter crash expected in approximately {0} minutes",
			["planecrash"] = "Next Plane crash expected in approximately {0} minutes",
			["removing"] = "Attempting to remove any current running event: {0}",
			["running"] = "Attempting to run automated event: {0}",
			["santa"] = "Next Santa's visit expected in approximately {0} minutes",
			["unknownevent"] = "Event type not for for {0}",
			["what"] = "no clue what {0} event is"
            }, this);
        }

        #region Config

        private ConfigData configData;
		private ConfigData DefaultconfigData;
        class ConfigData
        {
            public Dictionary<string, string> Settings { get; set; }
            public Dictionary<EventType, EventEntry> Events { get; set; }
        }

        class EventEntry
        {
            public  bool      Enabled { get; set; }
            public  bool      AnnounceNext { get; set; }
			public  bool      ClearOnStart { get; set; }
			public  int       MaximumNumber { get; set; }
			public  float     StartOffset { get; set; }
            public  int       MinimumTimeBetween { get; set; }
            public  int       MaximumTimeBetween { get; set; }
			[JsonIgnore]
            public  DateTime  NextRun { get; set; }
        }

        private void LoadVariables()
        {
			SetDefaultConfig();
            LoadConfigVariables();
            SaveConfig();
        }

         private void SetDefaultConfig()
        {
            var config = new ConfigData
            {
				Settings = new Dictionary<String, String>
				{
					{"AnnounceOnLoad","false"},
					{"UseGUIAnnouncementsPlugin","false"},
					{"TurnOffEvents","true"},
					{"RestartTimerOnDeath","true"},
					{"StartupDelay","60"}
				},
                Events = new Dictionary<EventType, EventEntry>
                {
                    { EventType.Bradley, new EventEntry
                    {
                        Enabled = false,
						AnnounceNext = false,
						ClearOnStart = false,
						MaximumNumber = 1,
						StartOffset = 0,
                        MinimumTimeBetween = 30,
                        MaximumTimeBetween = 45,
						NextRun = unsetDate
                    }
                    },
                    { EventType.CargoPlane, new EventEntry
                    {
                        Enabled = false,
						AnnounceNext = false,
						ClearOnStart = false,
						MaximumNumber = 1,
						StartOffset = 0,
                        MinimumTimeBetween = 30,
                        MaximumTimeBetween = 45,
						NextRun = unsetDate
                    }
                    },
                    { EventType.CargoShip, new EventEntry
                    {
                        Enabled = false,
						AnnounceNext = false,
						ClearOnStart = false,
						MaximumNumber = 1,
						StartOffset = 0,
                        MinimumTimeBetween = 30,
                        MaximumTimeBetween = 45,
						NextRun = unsetDate
                    }
                    },
                    { EventType.Chinook, new EventEntry
                    {
                        Enabled = false,
						AnnounceNext = false,
						ClearOnStart = false,
						MaximumNumber = 3,
						StartOffset = 0,
                        MinimumTimeBetween = 30,
                        MaximumTimeBetween = 45,
						NextRun = unsetDate
                    }
                    },
                    { EventType.Helicopter, new EventEntry
                    {
                        Enabled = false,
						AnnounceNext = false,
						ClearOnStart = false,
						MaximumNumber = 1,
						StartOffset = 0,
                        MinimumTimeBetween = 45,
                        MaximumTimeBetween = 60,
						NextRun = unsetDate
                    }
                    },
                    { EventType.HelicopterRefuel, new EventEntry
                    {
                        Enabled = false,
						AnnounceNext = false,
						ClearOnStart = false,
						MaximumNumber = 1,
						StartOffset = 0,
                        MinimumTimeBetween = 45,
                        MaximumTimeBetween = 60,
						NextRun = unsetDate
                    }
                    },
                    { EventType.PlaneCrash, new EventEntry
                    {
                        Enabled = false,
						AnnounceNext = false,
						ClearOnStart = false,
						MaximumNumber = 1,
						StartOffset = 0,
                        MinimumTimeBetween = 45,
                        MaximumTimeBetween = 60,
						NextRun = unsetDate
                    }
                    },
                    { EventType.PilotEject, new EventEntry
                    {
                        Enabled = false,
						AnnounceNext = false,
						ClearOnStart = false,
						MaximumNumber = 1,
						StartOffset = 0,
                        MinimumTimeBetween = 45,
                        MaximumTimeBetween = 60,
						NextRun = unsetDate
                    }
                    },
                    { EventType.XMasEvent, new EventEntry
                    {
                        Enabled = false,
						AnnounceNext = false,
						ClearOnStart = false,
						MaximumNumber = 1,
						StartOffset = 0,
                        MinimumTimeBetween = 60,
                        MaximumTimeBetween = 120,
						NextRun = unsetDate

                    }
                    },
                    { EventType.EasterEvent, new EventEntry
                    {
                        Enabled = false,
						AnnounceNext = false,
						ClearOnStart = false,
						MaximumNumber = 1,
						StartOffset = 0,
                        MinimumTimeBetween = 30,
                        MaximumTimeBetween = 60,
						NextRun = unsetDate
                    }
					},
                    { EventType.HalloweenEvent, new EventEntry
                    {
                        Enabled = false,
						AnnounceNext = false,
						ClearOnStart = false,
						MaximumNumber = 1,
						StartOffset = 0,
                        MinimumTimeBetween = 30,
                        MaximumTimeBetween = 60,
						NextRun = unsetDate
                    }
                    },
                    { EventType.SantaEvent, new EventEntry
                    {
                        Enabled = false,
						AnnounceNext = false,
						ClearOnStart = false,
						MaximumNumber = 1,
						StartOffset = 0,
                        MinimumTimeBetween = 30,
                        MaximumTimeBetween = 60,
						NextRun = unsetDate
                    }
                    }
                }
            };
			DefaultconfigData = config;
			configData = config;
        }

        protected override void LoadDefaultConfig()
        {
			SetDefaultConfig();
			SaveConfig(DefaultconfigData);
		}

        #endregion
        #region Core Methods
        private void OnServerInitialized()
        {
            permission.RegisterPermission(permAuto, this);
            permission.RegisterPermission(permNext, this);

			if (TurnOffEvents)
			{
				//Puts("Turned off Server Events.");
				rust.RunServerCommand("server.events false");

				foreach (EventSchedule schedule in GameObject.FindObjectsOfType<EventSchedule>().ToList())
				{
					//Puts($"Events {schedule} disabled");
					schedule.enabled = false;
				}

				//Puts("Disabled default Bradley.");
				rust.RunServerCommand("bradley.enabled false");

				foreach (BradleySpawner spawner in GameObject.FindObjectsOfType<BradleySpawner>().ToList())
				{
					//Puts($"Events {spawner} disabled");
					spawner.enabled = false;
					spawner.CancelInvoke("CheckIfRespawnNeeded");
					spawner.CancelInvoke("DelayedStart");
				}
			}
        }

		private void CleanEmUp()
		{
			if (configData.Events[EventType.Bradley].ClearOnStart)
				RemoveType(EventType.Bradley);

			if (configData.Events[EventType.CargoPlane].ClearOnStart)
				RemoveType(EventType.CargoPlane);

			if (configData.Events[EventType.CargoShip].ClearOnStart)
				RemoveType(EventType.CargoShip);

			if (configData.Events[EventType.Chinook].ClearOnStart)
				RemoveType(EventType.Chinook);

			if (configData.Events[EventType.Helicopter].ClearOnStart)
				RemoveType(EventType.Helicopter);
		}

		private void RemoveType(EventType type)
		{
			// if defaults off kill any existing ones
			if (type == EventType.Bradley)
			{
				foreach (var x in BaseNetworkable.serverEntities.OfType<BradleyAPC>().ToList())
				{
					Puts("Killing a Bradley");
					x.Kill();
				}
			}
			else if (type == EventType.CargoPlane || type == EventType.PlaneCrash)
			{
				foreach (var x in BaseNetworkable.serverEntities.OfType<CargoPlane>().ToList())
				{
					Puts("Killing a Cargo Plane");
					x.Kill();
				}
			}
			else if (type == EventType.CargoShip)
			{
				foreach (var x in BaseNetworkable.serverEntities.OfType<CargoShip>().ToList())
				{
					Puts("Killing a Cargo Ship");
					x.Kill();
				}
			}
			else if (type == EventType.Chinook)
			{
				foreach (var x in BaseNetworkable.serverEntities.OfType<CH47Helicopter>().ToList())
				{
					Puts("Killing a Chinook (CH47)");
					x.Kill();
				}
			}
			else if (type == EventType.Helicopter || type == EventType.PilotEject || type == EventType.HelicopterRefuel)
			{
				foreach (var x in BaseNetworkable.serverEntities.OfType<BaseHelicopter>().ToList())
				{
					Puts("Killing a Helicopter");
					x.Kill();
				}
			}
			else
				Puts("No remover defined for " + PrintType(type));
		}

		private void CheckPluginsandStart()
		{
			if (doGUIAnnouncements)
			{
				try
				{
					if (GUIAnnouncements.IsLoaded == true)
						doGUIAnnouncements = true;
					else
					{
						doGUIAnnouncements = false;
						PrintWarning("Warning: GUIAnnouncements plugin was not found! Messages will be sent directly to players.");
					}
				}
				catch
				{
					doGUIAnnouncements = false;
					PrintWarning("Warning: GUIAnnouncements plugin was not found! Messages will be sent directly to players.");
				}
			}

            foreach (var eventType in configData.Events)
			{
				// clear last run dates and spawned flag
				configData.Events[eventType.Key].NextRun = unsetDate;
				eventTimers[eventType.Key] = null;
				// if event is a plugin event make sure the plugin is present, otherwise disable the event
				if (eventType.Key == EventType.PlaneCrash)
					try
					{
						if (!PlaneCrash.IsLoaded)
							configData.Events[eventType.Key].Enabled = false;
					}
					catch
					{
						configData.Events[eventType.Key].Enabled = false;
					}
				else if (eventType.Key == EventType.HelicopterRefuel)
					try
					{
						if (!HeliRefuel.IsLoaded)
							configData.Events[eventType.Key].Enabled = false;
					}
					catch
					{
						configData.Events[eventType.Key].Enabled = false;
					}
				else if (eventType.Key == EventType.PilotEject)
					try
					{
						if (!PilotEject.IsLoaded)
							configData.Events[eventType.Key].Enabled = false;
					}
					catch
					{
						configData.Events[eventType.Key].Enabled = false;
					}

				// remove the old timer if it exists
				if (eventTimers[eventType.Key] != null)
				{
					try
					{
						eventTimers[eventType.Key].Destroy();
						eventTimers[eventType.Key] = null;
					}
					catch {Puts("Could not removed old timer for - " + PrintType(eventType.Key));}
				}

				if (configData.Events[eventType.Key].Enabled)
					eventTimers[eventType.Key] = timer.Once(StartupDelay+5, () => {StartEventTimer(eventType.Key,AnnounceOnLoad); return;});
			}
			SaveConfig(configData);
		}

		private void Loaded()
		{
			LoadVariables();
			SaveConfig(configData);
			// clean up old entites (not recommended)
			timer.Once(StartupDelay-10, () => {CleanEmUp(); return;});
			timer.Once(StartupDelay-30, () => {CheckPluginsandStart(); return;});
		}

        private void Unload()
        {
			if (TurnOffEvents)
			{
				//Puts("Turned on Server Events.");
				rust.RunServerCommand("server.events true");

				foreach (EventSchedule schedule in GameObject.FindObjectsOfType<EventSchedule>().ToList())
				{
					//Puts($"Events {schedule} enabled");
					schedule.enabled = true;
				}

				//Puts("Disabled default Bradley.");
				rust.RunServerCommand("bradley.enabled true");

				foreach (BradleySpawner spawner in GameObject.FindObjectsOfType<BradleySpawner>().ToList())
				{
					//Puts($"Events {spawner} enabled");
					spawner.enabled = true;
				}
			}

            foreach(var eTimer in eventTimers)
            {
                if (eTimer.Value != null)
				{
					try
					{
						eTimer.Value.Destroy();
					}
					catch
					{}

				}
            }
        }

		// reset the timer if the Bradley, Helicopter of Chinook are destroyed
		private void OnEntityDeath(BaseCombatEntity victim, HitInfo info)
		{
			// check if they want to restart the timer if the flag is set
			if (!RestartTimerOnDeath) return;

			EventType type = EventType.None;

			if (victim is BradleyAPC)
				type = EventType.Bradley;
			if (victim is BaseHelicopter)
				type = EventType.Helicopter;
			if (victim is CH47HelicopterAIController)
				type = EventType.Chinook;
			if (victim is CargoPlane)
				type = EventType.CargoPlane;
			if (victim is CargoShip)
				type = EventType.CargoShip;

			if (type != EventType.None) // Valid Death to process
			{
				if (info != null && type != EventType.CargoPlane && type != EventType.CargoShip)
				{
					StartEventTimer(type, configData.Events[type].AnnounceNext);
				}
			}
		}

        #endregion
        //#region Command
		[ChatCommand("nextevent")]
		private void chatAENext(BasePlayer player, string command, string[] args)
        {
			IPlayer iplayer = player.IPlayer;
            if (!IsAllowedNext(iplayer))
            {
                iplayer.Message(Lang("notallowed", iplayer.Id, command));
                return;
            }

			if (args == null || args.Length < 1 || String.IsNullOrWhiteSpace(args[0]))
			{
				iplayer.Message(Lang("blankevent", iplayer.Id));
				return;
			}
			else
			{
				if (args[0] == "*")
				{
					// do them all
					foreach (var Event in configData.Events)
						AENextShow(iplayer, Event.Key);
				}
				else
				{
					EventType eventtype = PickEvent(args[0].ToLower(),iplayer);
					if (eventtype == null || eventtype == EventType.None)
						iplayer.Message(Lang("unknownevent",args[0] , iplayer.Id));
					else
						AENextShow(iplayer, eventtype);

				}
			}
		}

		[ChatCommand("runevent")]
		private void chatAERun(BasePlayer player, string command, string[] args)
        {
			string[] pass_args  = args.Skip(1).ToArray();
			IPlayer iplayer = player.IPlayer;
            if (!IsAllowedCMD(iplayer))
            {
                iplayer.Message(Lang("notallowed", iplayer.Id, command));
                return;
            }

			if (args == null || args.Length < 1 || String.IsNullOrWhiteSpace(args[0]))
			{
				 iplayer.Message(Lang("blankevent", iplayer.Id));
				return;
			}
			else
			{
				iplayer.Message(Lang("running", iplayer.Id, args[0]));
				Puts("Player " + iplayer.Name + " (" + iplayer.Id.ToString() + ") is attempting to run automated event: " + args[0]);
				RunEvent(PickEvent(args[0].ToLower(),iplayer), pass_args, true);
			}
		}

		[ChatCommand("killevent")]
        private void ChatAERemoveType(BasePlayer player, string command, string[] args)
        {
			string[] pass_args  = args.Skip(1).ToArray();
			IPlayer iplayer = player.IPlayer;
            if (!IsAllowedCMD(iplayer))
            {
                iplayer.Message(Lang("notallowed", iplayer.Id, command));
                return;
            }

			if (args == null || args.Length < 1 || String.IsNullOrWhiteSpace(args[0]))
			{
				 iplayer.Message(Lang("blankevent", iplayer.Id));
				return;
			}
			else
			{
				iplayer.Message(Lang("removing", iplayer.Id, args[0].ToLower()));
				RemoveType(PickEvent(args[0],null));
			}
        }

		[ConsoleCommand("nextevent")]
		private void consoleAENext(ConsoleSystem.Arg arg)
        {
			if (arg?.Args == null || arg?.Args?.Length < 1 || String.IsNullOrWhiteSpace(arg.Args[0]))
			{
				Puts("No event specified!");
				return;
			}
			else
			{
				//Puts("Next Event processing Type: " + arg.Args[0]);
				if (arg.Args[0] == "*")
				{
					// do them all
					foreach (var Event in configData.Events)
						AENextShowConsole(Event.Key);
				}
				else
				{
					EventType eventtype = PickEvent(arg.Args[0],null);
					if (eventtype == null || eventtype == EventType.None)
						Puts("Unknown Type: " + arg.Args[0]);
					else
						AENextShowConsole(eventtype);
				}
			}
		}

        [ConsoleCommand("runevent")]
        private void consoleAERunEvent(ConsoleSystem.Arg arg)
        {
			if (arg?.Args == null || arg?.Args?.Length < 1 || String.IsNullOrWhiteSpace(arg.Args[0]))
			{
				Puts("No event specified!");
				return;
			}
			else
			{
				string[] pass_args  = arg?.Args.Skip(1).ToArray();
				//Puts ("Running Automated Event: " + arg.Args[0].ToLower());
				RunEvent(PickEvent(arg.Args[0],null), pass_args, true);
			}
		}

		[ConsoleCommand("killevent")]
        private void ConsoleAERemoveType(ConsoleSystem.Arg arg)
        {
			if (arg?.Args == null || arg?.Args?.Length < 1 || String.IsNullOrWhiteSpace(arg.Args[0]))
			{
				Puts("No event specified!");
				return;
			}
			else
			{
				Puts ("Removing Event (If present): " + arg.Args[0].ToLower());
				RemoveType(PickEvent(arg.Args[0],null));
			}
        }

        //#endregion
        #region Functions
		private void AENextShowConsole(EventType Event)
        {
			if (!configData.Events[Event].Enabled)
				Puts(Lang("notset", "76561198068046723",PrintType(Event)));
			DateTime localDate = System.DateTime.Now;
			TimeSpan deltaDate = new TimeSpan(0, 0, 0, 0, 0);
			int intMinutes     = 0;

			deltaDate = configData.Events[Event].NextRun - localDate;
			intMinutes = (int)deltaDate.TotalMinutes;
			//Puts(Lang(PrintType(Event).ToLower().Replace(" ",""), "76561198068046723" , intMinutes.ToString()));
			//Puts(PrintType(Event).ToLower().Replace(" ",""));
			if (intMinutes >= 0)
				Puts(String.Concat(Lang(PrintType(Event).ToLower().Replace(" ",""), "76561198068046723" , intMinutes.ToString())));
		}

		private void AENextShow(IPlayer iplayer, EventType Event)
        {
			if (!configData.Events[Event].Enabled)
				iplayer.Message(Lang("notset", iplayer.Id,PrintType(Event)));
			DateTime localDate = System.DateTime.Now;
			TimeSpan deltaDate = new TimeSpan(0, 0, 0, 0, 0);
			int intMinutes     = 0;

			deltaDate = configData.Events[Event].NextRun - localDate;
			intMinutes = (int)deltaDate.TotalMinutes;
			//Puts(PrintType(Event).ToLower().Replace(" ",""));
			if (intMinutes >= 0)
				iplayer.Message(String.Concat(Lang(PrintType(Event).ToLower().Replace(" ",""), iplayer.Id, intMinutes.ToString())));
		}

        private void StartEventTimer(EventType type, bool announce=true)
        {
			if (type == null || type == EventType.None)
				return;

            var config = configData.Events[type];
			// remove the old timer if it exists
			try
			{
				eventTimers[type].Destroy();
				eventTimers[type] = null;
			}
			catch {Puts("Could not removed old timer for " + PrintType(type));}

            if (!config.Enabled)
			{
				Puts("Not Running " + PrintType(type));
				return;
			}
			else
			{
				// allow for idiots
				float randomTime = 0;
				if (config.MinimumTimeBetween <= config.MaximumTimeBetween)
					randomTime = Oxide.Core.Random.Range(config.MinimumTimeBetween, config.MaximumTimeBetween);
				else
					randomTime = Oxide.Core.Random.Range(config.MaximumTimeBetween, config.MinimumTimeBetween);

				// apply start offset
				randomTime = randomTime + config.StartOffset;

				DateTime NextDate    = System.DateTime.Now.AddMinutes(randomTime);
				float GameHM         = TOD_Sky.Instance.Cycle.Hour;
				int   CurrentHours   = System.DateTime.Now.Hour;
				int   CurrentMinutes = System.DateTime.Now.Minute;
				float CurrentHM      = CurrentHours + (CurrentMinutes/60);
				int   NextHours      = NextDate.Hour;
				int   NextMinutes    = NextDate.Minute;
				float NextHM         = NextHours + (NextMinutes/60);

				config.NextRun = NextDate;
				string[] pass_args  = Array.Empty<string>();

				eventTimers[type] = timer.In(randomTime * 60, () => RunEvent(type, pass_args, false));
				Puts("Next " + PrintType(type) + " in " + randomTime.ToString() + " minutes");
				if (announce && config.AnnounceNext)
					MessagePlayers(PrintType(type).ToLower().Replace(" ",""),randomTime.ToString());
			}
        }

        private void RunEvent(EventType type, string[] pass_args, bool runonce=true)
        {

			//Puts("Running Event: " + PrintType(type) + " " + System.DateTime.Now.ToString());
			if (type == null || type == EventType.None)
				return;

			bool   used_other     = false;
            string prefabName     = string.Empty;
			float  y_extra_offset = 0.0f;
			string parms          = string.Empty;

			// get the parameters
			if (pass_args != null && pass_args.Length > 0)
			{
				parms = string.Join(" ", pass_args);
				parms = parms.Trim();
				if (string.IsNullOrEmpty(parms))
					parms = string.Empty;
				else
					parms = parms + " ";
			}

			//Puts(ConVar.Server.worldsize.ToString());
			float ran_min    =  0.75f;
			float ran_max    =  0.85f;
			float half_world = ConVar.Server.worldsize / 2;
			//Puts("half_world: " + half_world.ToString());

			// this might push this too close to the edge on some maps
			if (type == EventType.CargoShip)
			{
				ran_min = 0.87f;
				ran_max = 0.93f;
			}

			float x_plus_minus = ((UnityEngine.Random.value)>=0.50f)?-1.0f:1.0f;
			float z_plus_minus = ((UnityEngine.Random.value)>=0.50f)?-1.0f:1.0f;

			//Puts("x_plus_minus: " + x_plus_minus.ToString());
			//Puts("z_plus_minus: " + z_plus_minus.ToString());

			// safety check in case the sign trick fails
			if (x_plus_minus == 0) x_plus_minus = 1.0f;
			if (z_plus_minus == 0) z_plus_minus = -1.0f;

			Vector3 vector3_1 = new Vector3();
			vector3_1.x = Oxide.Core.Random.Range(ran_min, ran_max) * x_plus_minus * half_world;
			vector3_1.z = Oxide.Core.Random.Range(ran_min, ran_max) * z_plus_minus * half_world;

			if (type == EventType.CargoPlane || type == EventType.Chinook || type == EventType.Helicopter)
			{
				vector3_1.y  = 300.0f;
			}
			else if (type == EventType.CargoShip)
				vector3_1.y = 10.0f;
			else
				vector3_1.y = 200.0f;

	        //Puts("X1: " + vector3_1.x.ToString());
	        //Puts("Z1: " + vector3_1.z.ToString());
	        //Puts("Y1: " + vector3_1.y.ToString());

	        switch (type)
            {
                case EventType.Bradley:
	                if (BaseNetworkable.serverEntities.OfType<BradleyAPC>().ToList().Count >= configData.Events[EventType.Bradley].MaximumNumber)
	                {
		                Puts(" Bradley(s) already out");
	                }
					else
	                {
						try
						{
							if (BradleyControl.IsLoaded)
							{
								Puts("Spawning Bradley Control Bradley");
								if (string.IsNullOrEmpty(parms))
								{
									rust.RunServerCommand("bradleycontrol.reset");
								}
								else
								{
									rust.RunServerCommand("bradleycontrol.reset " + parms);
								}

								used_other = true;
							}
						}
						catch
						{
							used_other = false;
						}

						if (used_other == false)
						{
							Puts("Spawning Bradley");
							prefabName = "assets/prefabs/npc/m2bradley/bradleyapc.prefab";
							BradleyAPC bradley = (BradleyAPC)GameManager.server.CreateEntity(prefabName, new Vector3(), new Quaternion(), true);
							bradley.Spawn();
							// Ensures there is a AI path to follow.
							Vector3 position = BradleySpawner.singleton.path.interestZones[Oxide.Core.Random.Range(0, BradleySpawner.singleton.path.interestZones.Count)].transform.position;
							bradley.transform.position = position;
							bradley.DoAI = true;
							bradley.DoSimpleAI();
							bradley.InstallPatrolPath(BradleySpawner.singleton.path);
							bradley.CancelInvoke("CheckIfRespawnNeeded");
						}
					}
                    break;
                case EventType.CargoPlane:
					if (BaseNetworkable.serverEntities.OfType<CargoPlane>().ToList().Count >= configData.Events[EventType.CargoPlane].MaximumNumber)
					{
						Puts(" CargoPlane(s) already out");
					}
					else
					{
						try
						{
							Puts("Spawning Fancy Drop Cargo Plane");
							if (FancyDrop.IsLoaded == true)
							{
								if (pass_args.Length == 0)
								{
									if (string.IsNullOrEmpty(parms))
										rust.RunServerCommand("ad.random");
									else
										rust.RunServerCommand("ad.random " + parms);

								}
								else
								{
									if (string.IsNullOrEmpty(parms))
										rust.RunServerCommand("ad.toplayer");
									else
										rust.RunServerCommand("ad.toplayer " + parms);
								}
								used_other = true;
							}
						}
						catch
						{
							used_other = false;
						}
						if (used_other == false)
						{
							Puts("Spawning Cargo Plane");
							prefabName = "assets/prefabs/npc/cargo plane/cargo_plane.prefab";
							var Plane = (CargoPlane)GameManager.server.CreateEntity(prefabName, vector3_1, new Quaternion(), true);
							Plane.Spawn();
						}
					}
                    break;
                case EventType.CargoShip:
					if (BaseNetworkable.serverEntities.OfType<CargoShip>().ToList().Count >= configData.Events[EventType.CargoShip].MaximumNumber)
					{
						Puts(" CargoShip(s) already out");
					}
					else
					{
						Puts("Spawning CargoShip");
						prefabName = "assets/content/vehicles/boats/cargoship/cargoshiptest.prefab";
						var Ship = (CargoShip)GameManager.server.CreateEntity(prefabName, vector3_1, new Quaternion(), true);
						Ship.Spawn();
						// comment out previous three, and uncomment the next line if the cargoship is buggy
						//rust.RunServerCommand("spawn cargoshiptest");
					}
                    break;
                case EventType.Chinook:
					if (BaseNetworkable.serverEntities.OfType<CH47HelicopterAIController>().ToList().Count >= configData.Events[EventType.Chinook].MaximumNumber)
					{
						Puts(" Chinook(s) already out");
					}
					else
					{
						try
						{
							if (HeliControl.IsLoaded == true)
							{
								Puts("Spawning HeliControl Chinook");
								if (string.IsNullOrEmpty(parms))
									rust.RunServerCommand("callch47");
								else
									rust.RunServerCommand("callch47 "  + parms);
								used_other = true;
							}
						}
						catch
						{
							used_other = false;
						}
						if (used_other == false)
						{
							Puts("Spawning Chinook");
							prefabName = "assets/prefabs/npc/ch47/ch47scientists.entity.prefab"; // "assets/prefabs/npc/ch47/ch47.entity.prefab";
							var Chin = (CH47HelicopterAIController)GameManager.server.CreateEntity(prefabName, vector3_1, new Quaternion(), true);
							Chin.Spawn();
						}
					}
                    break;
                case EventType.Helicopter:
					if (BaseNetworkable.serverEntities.OfType<BaseHelicopter>().ToList().Count >= configData.Events[EventType.Helicopter].MaximumNumber)
					{
						Puts(" Chinook(s) already out");
					}
					else
					{
						try
						{
							if (HeliControl.IsLoaded == true)
							{
								Puts("Spawning HeliControl Helicopter");
								if (string.IsNullOrEmpty(parms))
									rust.RunServerCommand("callheli");
								else
									rust.RunServerCommand("callheli " + parms);
								used_other = true;
							}
						}
						catch
						{
							used_other = false;
						}
						if (used_other == false)
						{
							Puts("Spawning Helicopter");
							prefabName = "assets/prefabs/npc/patrol helicopter/patrolhelicopter.prefab";
							var Heli = (BaseHelicopter)GameManager.server.CreateEntity(prefabName, vector3_1, new Quaternion(), true);
							Heli.Spawn();
						}
					}
					break;
                case EventType.PlaneCrash:
					if (PlaneCrash.IsLoaded == true)
					{
						Puts("Spawning Plane Crash");
						if (string.IsNullOrEmpty(parms))
							rust.RunServerCommand("callcrash");
						else
							rust.RunServerCommand("callcrash " + parms);
						used_other = true;
					}
					else
					{
						Puts("Plane Crash is not loaded");
						configData.Events[EventType.PlaneCrash].Enabled = false;
						used_other = false;
					}

                    break;
                case EventType.HelicopterRefuel:
					if (HeliRefuel.IsLoaded == true)
					{
						Puts("Spawning Heli Refuel");
						if (string.IsNullOrEmpty(parms))
							rust.RunServerCommand("hr call");
						else
							rust.RunServerCommand("hr call " + parms);
					}
                    break;
                case EventType.PilotEject:
					if (PilotEject.IsLoaded == true)
					{
						try
						{
							Puts("Spawning Pilot Eject");
							if (string.IsNullOrEmpty(parms))
								rust.RunServerCommand("pe call");
							else
								rust.RunServerCommand("pe call " + parms);
							used_other = true;
						}
						catch
						{
							configData.Events[EventType.PilotEject].Enabled = false;
							used_other = false;
						}
					}
					else
					{
						configData.Events[EventType.PilotEject].Enabled = false;
						used_other = false;
					}

                    break;
                case EventType.SantaEvent:
					{
						Puts("Santa is coming, have you been good?");
						rust.RunServerCommand("spawn santasleigh");
						break;
					}
                case EventType.XMasEvent:
					try
					{
						if (AlphaChristmas.IsLoaded == true)
						{
							Puts("Running AlphaChristmas refill");
							rust.RunServerCommand("alphachristmas.refill");
							used_other = true;
						}
					}
					catch
					{
						used_other = false;
					}
					if (used_other == false)
					{
						Puts("Christmas Refill is occuring");
						rust.RunServerCommand("xmas.refill");
					}
                    break;
                case EventType.EasterEvent:
					{
						// Thank you to Death for this command!
						Puts("Happy Easter Egg Hunt is occuring");
						rust.RunServerCommand("spawn egghunt");
						break;
					}
                case EventType.HalloweenEvent:
					{
						Puts("Spooky Halloween Hunt is occuring");
						rust.RunServerCommand("spawn halloweenhunt");
						break;
					}
				default:
					break;
            }
			if (!runonce)
			{
				var config = configData.Events[type];
				StartEventTimer(type,config.AnnounceNext);
			}
        }
        #endregion

        #region Helpers
        private EventType PickEvent(string Event, IPlayer iplayer)
		{
			string eventstring = Event.ToLower().Replace(" ","");
			//Puts("eventstring: " + eventstring);

			if (eventstring.Contains("brad"))
				return EventType.Bradley;
			else if (eventstring.Contains("refuel") || eventstring.Contains("hr"))
				return EventType.HelicopterRefuel;
			else if (eventstring.Contains("helicrash") || eventstring.Contains("eject") || eventstring.Contains("pe"))
				return EventType.PilotEject;
			else if (eventstring.Contains("planecrash") || eventstring.Contains("crash"))
				return EventType.PlaneCrash;
			else if (eventstring.Contains("heli") || eventstring.Contains("copter"))
				return EventType.Helicopter;
			else if (eventstring.Contains("plane") || eventstring.Contains("plane"))
				return EventType.CargoPlane;
			else if (eventstring.Contains("ship"))
				return EventType.CargoShip;
			else if (eventstring.Contains("ch47") || eventstring.Contains("chin"))
				return EventType.Chinook;
			else if (eventstring.Contains("xmas") || eventstring.Contains("chris") || eventstring.Contains("yule"))
				return EventType.XMasEvent;
			else if (eventstring.Contains("santa") || eventstring.Contains("nick") || eventstring.Contains("wodan"))
				return EventType.SantaEvent;
			else if (eventstring.Contains("easter") || eventstring.Contains("egg") || eventstring.Contains("bunny"))
				return EventType.EasterEvent;
			else if (eventstring.Contains("hall") || eventstring.Contains("spooky") || eventstring.Contains("candy") || eventstring.Contains("samhain"))
				return EventType.HalloweenEvent;
			else
			{
				if (iplayer != null && iplayer.Id != null)
						iplayer.Message(Lang("what", iplayer.Id, Event));
				else
						Puts("No clue what event this is: " + Event);
				return EventType.None;
			}
		}

		private string PrintType(EventType type)
		{
			if (type == null || type == EventType.None)
				return "No Type Specified";
			else
			{
				switch (type)
				{
					case EventType.None:
					{
						return "None";
						break;
					}
					case EventType.Bradley:
					{
						return "Bradley";
						break;
					}
					case EventType.CargoPlane:
					{
						return "Cargo Plane";
						break;
					}
					case EventType.CargoShip:
					{
						return "Cargo Ship";
						break;
					}
					case EventType.Chinook:
					{
						return "Chinook";
						break;
					}
					case EventType.Helicopter:
					{
						return "Helicopter";
						break;
					}
					case EventType.HelicopterRefuel:
					{
						return "Helicopter Refuel";
						break;
					}
					case EventType.PlaneCrash:
					{
						return "Plane Crash";
						break;
					}
					case EventType.PilotEject:
					{
						return "Pilot Eject";
						break;
					}
					case EventType.SantaEvent:
					{
						return "Santa";
						break;
					}
					case EventType.XMasEvent:
					{
						return "Christmas";
						break;
					}
					case EventType.EasterEvent:
					{
						return "Easter";
						break;
					}
					case EventType.HalloweenEvent:
					{
						return "Halloween";
						break;
					}
					default:
						return "Unknown Type";
						break;
				}
			}
		}

        private void LoadConfigVariables()
		{
			configData = Config.ReadObject<ConfigData>();
			try
			{
				AnnounceOnLoad      = Convert.ToBoolean(configData.Settings["AnnounceOnLoad"]);
				doGUIAnnouncements  = Convert.ToBoolean(configData.Settings["UseGUIAnnouncementsPlugin"]);
				RestartTimerOnDeath = Convert.ToBoolean(configData.Settings["RestartTimerOnDeath"]);
				TurnOffEvents       = Convert.ToBoolean(configData.Settings["TurnOffEvents"]);
				try
				{
					StartupDelay   = Int32.Parse(configData.Settings["StartupDelay"]);
				}
				catch
				{
					StartupDelay   = 60;
					PrintWarning("Warning: Startup delay not set, defaulting.");
				}
			}
			catch
			{
				AnnounceOnLoad      = true;
				doGUIAnnouncements  = false;
				RestartTimerOnDeath = true;
				TurnOffEvents       = true;
				StartupDelay        = 60;
				PrintWarning("Warning: issue loading your configuration, defaulting.");
			}

			// set these in case they were missing or changed
			try
			{
				configData.Settings["AnnounceOnLoad"]            = Convert.ToString(AnnounceOnLoad);
				configData.Settings["UseGUIAnnouncementsPlugin"] = Convert.ToString(doGUIAnnouncements);
				configData.Settings["RestartTimerOnDeath"]       = Convert.ToString(RestartTimerOnDeath);
				configData.Settings["TurnOffEvents"]             = Convert.ToString(TurnOffEvents);
				configData.Settings["StartupDelay"]              = Convert.ToString(StartupDelay);
			}
			catch
			{
				PrintWarning("Warning: issue proccessing your configuration data, defaulting.");
			}
		}

		private void MessagePlayers(string message, string minutes)
		{
			string mess = " ";

			if (doGUIAnnouncements && GUIAnnouncements.IsLoaded)
				foreach (var player in BasePlayer.activePlayerList)
				{
					mess = String.Concat(Lang(message,player.UserIDString, minutes));
					if (!String.IsNullOrWhiteSpace(mess))
					{
						//rust.RunServerCommand("announce.announceto "+player.UserIDString.Quote()+ " "+mess.Quote());
						try
						{
							GUIAnnouncements?.Call("CreateAnnouncement", mess, "Purple", "White", player);
						}
						catch
						{
							doGUIAnnouncements = false;
							Puts("Error in call to GUIAnnouncements");
						}
					}
				}
			else
				foreach (var player in BasePlayer.activePlayerList)
				{
					mess = String.Concat(Lang(message, player.UserIDString, minutes));
					if (!String.IsNullOrWhiteSpace(mess))
						SendReply(player, mess);
				}
		}

        void SaveConfig(ConfigData config) => Config.WriteObject(config, true);
        private T GetConfig<T>(string name, T defaultValue) => Config[name] == null ? defaultValue : (T) Convert.ChangeType(Config[name], typeof (T));
        private string Lang(string key, string id = null, params object[] args) => string.Format(lang.GetMessage(key, this, id), args);
		private bool IsAllowedCMD(IPlayer iplayer) {return iplayer != null && (iplayer.IsAdmin || iplayer.HasPermission(permAuto));}
		private bool IsAllowedNext(IPlayer iplayer) {return iplayer != null && (iplayer.IsAdmin || iplayer.HasPermission(permNext));}
        #endregion
    }
}