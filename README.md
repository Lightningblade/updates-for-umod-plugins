#Updates for UMod plugins
If you want to add a plugin, drop it in to the repo and exclude it from the wildcard in the gitignore by apending !<filename.cs>

If you have your own server to test with I recommend using 
```git clone git@gitlab.com:Lightningblade/updates-for-umod-plugins.git plugins```
in the oxide folder. With the gitignore it will ignore all other plugins.